"""A library for manipulating and analyzing audio signals"""

import re
import os

import numpy as np
from scipy.signal import convolve, gaussian
import scipy
from sklearn.mixture import GMM
import echonest.remix.audio as audio

from settings import (NOTE_LIBRARY_FILEPATHNAMES, STANDARD_WINDOW_LENGTH,
                        STANDARD_SAMPLE_RATE)


def import_notes(filepathnames=NOTE_LIBRARY_FILEPATHNAMES):
    """
    INPUT: directory and filename format, like "/Notes/*.wav"
    OUTPUT: list of AudioData objects

    Note that only the left channel is returned
    """
    samples = []
    for filepathname in filepathnames:
        audio_data = audio.AudioData(filepathname)
        sample = audio_data.data[:,0]
        sample_rate = audio_data.sampleRate
        filename = re.findall('[\da-zA-z]*[.]wav', filepathname)[0]
        sample = audio.AudioData(ndarray=sample,
                                 sampleRate = sample_rate,
                                 filename=filename)
        samples.append(sample)
    return samples

def import_recording(filepathname):
    """
    INPUT: filepathname of wav or mp3 file
    OUTPUT: AudioData object containing left channel of input .wav file
    """
    audio_data = audio.AudioData(filepathname)
    sample = audio_data.data[:,0]
    sample_rate = audio_data.sampleRate
    filename = re.findall('[\da-zA-z]*[.]wav', filepathname)[0]
    sample = audio.AudioData(ndarray=sample,
                             sampleRate = sample_rate,
                             filename=filename)
    return sample

def split_recording(recording, discard_noise=True):
    '''Splits a recording into individiual music notes
    
    Optionally, the segments containing only noise can be discarded
    
    INPUT: AudioData object containing recording of individual music notes
    OUTPUT: list of AudioData objects containing separate music notes
    '''
    def find_lowest_n(signal, window, n):
        '''
        takes signal thats positive and convolved with gaussian
        '''
        min_indices = np.array([], dtype=int)
        index = 0
        while index+window < len(signal):
            segment = signal[index:index+window]
            indices = segment.argsort() + index
            min_indices = np.concatenate((min_indices, indices[0:n]))
            index += int(round(window*.25))
        min_indices.sort()
        indices_to_delete = np.array([]) # delete all indices that are consecutive and above local min
        i = 0
        while i+1 < len(min_indices):
            if min_indices[i] in (min_indices[i+1] - 1, min_indices[i+1]):
                indices_to_delete = np.append(indices_to_delete, i+1)
            i += 1
        min_indices = np.delete(min_indices, indices_to_delete)
        return min_indices

    def segment_recording(recording, breaks):
        '''Splits a recording at the specified break points
        
        INPUT:
            recording, an AudioData object containing notes
            breaks, a list of indices specifying where the recording should be split
        OUTPUT: 
            list of separate AudioData objects
        '''
        segments = []
        for i in xrange(len(breaks)-1):
            segment = audio.AudioData(ndarray=recording.data[breaks[i]:breaks[i+1]],
                                     sampleRate=recording.sampleRate,
                                     filename=recording.filename)
            segments.append(segment)
        return segments

    def discard_noise_segments(segments):
        '''Uses gaussian mixture model to distinguish between notes and noise based on length
        
        INPUT: list of AudioData objects
        OUTPUT: list of AudioData objects, excluding noise segments
        '''
        # fit gaussian mixture
        gmm = GMM(n_components=2)
        lengths = [len(s) for s in segments]
        X = np.array(lengths)[:,np.newaxis]
        gmm.fit(X)
        labels = gmm.predict(X)
        
        # use gaussian mixture model to distinguish between 
        # noise (short samples) and music notes (long samples)
        mean0 = np.mean(X[labels==0])
        mean1 = np.mean(X[labels==1])
        keep_label = int(mean1>mean0)
        keep_segments = []
        for index in np.where(labels==keep_label)[0]:
            keep_segments.append(segments[index])
            
        return keep_segments

    # convolve the signal with a gaussian to make points between notes clearer
    window = scipy.signal.gaussian(2000, 1000)
    convolved = scipy.signal.convolve(abs(recording.data), window, mode='same')
    
    # identify the points between notes
    breaks = find_lowest_n(convolved, 44100/2, 2)
    
    # segment the audio between the notes
    segments = segment_recording(recording, breaks)
    
    # optionally, discard the segments of audio that contain only noise
    if discard_noise:
        segments = discard_noise_segments(segments)
        
    return segments

def standardize_length(notes, length=None):
    '''Zero-pads a collection of audio segments (notes) to make them all the same length'''
    if length == None:
        length = min(len(note) for note in notes)
    for note in notes:
        print len(note)
        if len(note.data) <= length:
            num_zeros = length - len(note.data)
            note.data = np.append(note.data, [0.]*num_zeros)
        else:
            note.data = note.data[0:length]
    return notes

def build_library(directory, discard_noise=True):
    '''
    INPUT: directory
    OUTPUT: dict (notename => list of AudioData objects)

    -imports all .wav files in directory
    -- file names must be in this format: [note_name].wav, e.g. "A440.wav"
    -splits recording into separate notes
    -optionally, discards noise samples
    -returns dictionary of notes organized by note name
    '''
    # directory = '/Users/michaeljancsy/Projects/Bela/data/long notes - raw'
    filenames = os.listdir(directory)
    library = {}
    for filename in filenames:
        if filename.endswith('.wav'):
            note_name = filename[:-len('.wav')]
            recording = import_recording(os.path.join(directory, filename))
            notes = split_recording(recording, discard_noise=discard_noise)
            library[note_name] = notes
    return library

def select_basis_from_library(library, return_names=False):
    '''
    INPUT: dict, library of notes
    OUTPUT: list of notes, optionally note names

    returns list of first sample of each note in library,
    sorted by note names
    '''
    basis_note_names = sorted(note_name for note_name in library)
    basis_notes = [library[note_name][0] for note_name in basis_note_names]
    basis_notes = standardize_length(basis_notes, length=44100/2)
    if not return_names:
        return basis_notes
    else:
        return basis_notes, basis_note_names

def make_chord_from_samples(samples, target_length=None):
    '''
    INPUT: iterable of AudioData objects representing waveforms
    OUTPUT: AudioData consisting of all input waveforms played simultaneously

    All AudioData must have same sample rate.

    If target_length is specified, sample will be zero-padded/truncated to
    match target_length.
    '''
    sample_rate = samples[0].sampleRate
    min_length = min([len(s.data) for s in samples])
    chord = np.zeros(shape=min_length)
    for s in samples:
        chord += s.data[:min_length]
    if target_length is not None:
        if len(chord) < target_length:
            chord = np.append(chord, np.zeros(target_length - len(chord)))
        elif len(chord) > target_length:
            chord = chord[:target_length]
    chord = audio.AudioData(ndarray=chord, sampleRate=sample_rate)
    return chord

def make_chord_from_library(library, note_names, indices=None):
    '''
    INPUT:
    - library: dict (note_name => list of AudioData objects)
    - note_names: iterable of strings, e.g. ['A440']
    - indices (optional): index for intended sample note. Default index is 0.
    OUTPUT:
    - chord: AudioData

    Combines notes from library into chord
    '''
    if indices is None:
        indices = [0]*len(note_names)
    elif isinstance(indices, int):
        indices=[indices]*len(note_names)
        print indices
    notes = []
    for index, note_name in enumerate(note_names):
        notes.append(library[note_name][index])
    print notes, len(notes)
    chord = make_chord_from_samples(notes)
    return chord


def randomly_sample_chords_with_missing_notes(notes,
                                              degree,
                                              sample_size,
                                              target_length=\
                                                STANDARD_WINDOW_LENGTH):
    """
    INPUT:
    notes - iterable of AudioData objects containing single notes
    degree - degree of chord (e.g. 3 for triads)
    sample_size - desired number of sample chords
    target_length - length of chord in samples, optional

    OUTPUT:
    list of AudioData objects containing chords
    """
    # randomly sample integers between 0 and "len(notes) choose degree"
    max_int = sc.misc.comb(len(notes), degree) # len(notes) choose degree (n choose k)
    integers = np.arange(max_int, dtype=int)
    np.random.shuffle(integers)
    indices = set(integers[:sample_size])
    # generate chords
    note_combinations = combinations(notes, degree)
    # degree + 1 = number of missing note chords + 1 full chord
    sample_chords = []
    index = 0
    for note_combo in note_combinations:
        if index in indices:
            chord_group = []
            chord_group.append(make_chord(note_combo, target_length))
            missing_note_combinations = combinations(note_combo, degree-1)
            for missing_note_combo in missing_note_combinations:
                partial_chord = make_chord(missing_note_combo, target_length)
                chord_group.append(partial_chord)
            sample_chords.append(chord_group)
        index += 1
    return sample_chords

def zero_pad(samples, target_length=None):
    """
    INPUT:
    notes - iterable of AudioData objects containing single notes
    target_length - intended number of samples in each note

    OUTPUT:
    numpy array (len(notes) x target_length)
    """
    if target_length is None:
        target_length = max([len(sample) for sample in samples])
    normalized_samples = np.empty((0,target_length))
    for sample in samples:
        zeros = np.zeros(target_length-len(sample))
        normalized_sample = np.hstack((sample,
                                       zeros))
        normalized_samples = np.vstack((normalized_samples,
                                       normalized_sample))
    return normalized_samples

def magnitude_spectra(notes,
                  sample_rate=STANDARD_SAMPLE_RATE,
                  return_ndarray=True):
    spectra = []
    for note in notes:
        freqs = np.fft.fftfreq(len(note), 1./sample_rate)
        positive_freq_indices = np.where(freqs>0)
        freqs = freqs[positive_freq_indices]
        fourier_coefs = np.fft.fft(note)
        magnitude_spectrum = np.abs(fourier_coefs.real[positive_freq_indices])
        spectra.append(magnitude_spectrum)
    if return_ndarray:
        spectra = np.vstack(spectra)
    return spectra, freqs

def power_spectra(notes,
                  sample_rate=STANDARD_SAMPLE_RATE,
                  return_ndarray=True):
    spectra, freqs = magnitude_spectra(notes,
                                       sample_rate,
                                       return_ndarray)
    if return_ndarray:
        spectra = spectra**2
    else:
        for i, s in enumerate(spectra):
            spectra[i] = spectra[i]**2
    return spectra, freqs

def rolling_window(a, window, hop_size):
    '''adapted from http://www.rigtorp.se/2011/01/01/rolling-statistics-numpy.html'''
    shape = (a.shape[0] - window + 1, window)
    strides = a.strides + (a.strides[0],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)[::hop_size]